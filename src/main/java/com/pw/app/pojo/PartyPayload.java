/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pw.app.pojo;

import java.util.List;

/**
 *
 * @author buls
 */
public class PartyPayload {
   
    private List<AppParty> parties;
    
    public PartyPayload() {
    
    }

    public List<AppParty> getParties() {
        return parties;
    }

    public void setParties(List<AppParty> parties) {
        this.parties = parties;
    }
        
}
