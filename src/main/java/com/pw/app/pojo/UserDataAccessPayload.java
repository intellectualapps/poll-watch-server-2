/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pw.app.pojo;

import java.util.List;

/**
 *
 * @author buls
 */
public class UserDataAccessPayload {

    private AppUser user;
    private List<AppDataAccess> electionEvents;
    private List<AppParty> parties;
    
    public UserDataAccessPayload() {
    
    }

    public AppUser getUser() {
        return user;
    }

    public void setUser(AppUser user) {
        this.user = user;
    }        

    public List<AppDataAccess> getElectionEvents() {
        return electionEvents;
    }

    public void setElectionEvents(List<AppDataAccess> electionEvents) {
        this.electionEvents = electionEvents;
    }

    public List<AppParty> getParties() {
        return parties;
    }

    public void setParties(List<AppParty> parties) {
        this.parties = parties;
    }    
    
}
