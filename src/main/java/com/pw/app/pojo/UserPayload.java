/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pw.app.pojo;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author buls
 */
public class UserPayload {

    private List<AppUser> users;
    
    public UserPayload() {
        users = new ArrayList<AppUser>();
    }

    public List<AppUser> getUsers() {
        return users;
    }

    public void setUsers(List<AppUser> users) {
        this.users = users;
    }        
    
}
