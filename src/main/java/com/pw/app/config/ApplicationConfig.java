/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pw.app.config;

import com.pw.app.service.PartyScoreService;
import com.pw.app.service.PartyService;
import com.pw.app.service.PhotoService;
import com.pw.app.service.ProjectService;
import com.pw.app.service.UserService;
import com.pw.app.util.exception.GeneralAppExceptionMapper;
import java.util.HashSet;
import java.util.Set;
import javax.ws.rs.core.Application;

/**
 *
 * @author Lateefah
 */
@javax.ws.rs.ApplicationPath("/api")

public class ApplicationConfig extends Application{
    @Override    
    public Set<Class<?>> getClasses() {
        Set<Class<?>> s = new HashSet<Class<?>>();
        
        s.add(UserService.class);
        s.add(ProjectService.class);
        s.add(PartyScoreService.class);
        s.add(PartyService.class);
        s.add(PhotoService.class);
        s.add(GeneralAppExceptionMapper.class);
        
        return s;
    }
}
