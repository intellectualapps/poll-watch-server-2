package com.pw.app.http;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.LinkedHashMap;
import java.util.Map;


public class ServiceConsumer {

 
    private final String PHOTO_RESOURCE_ENDPOINT = "http://104.248.86.53/projects/update.json";
 
    private final String USER_AGENT = "Mozilla/5.0";
        private final String ID = "id";
        private final String RESULT_SHEET_PHOTO = "photo";
        private final String ELECTION_PHOTO_1 = "other_photo_1";
        private final String ELECTION_PHOTO_2 = "other_photo_2";
        private final String EXTRA_PHOTO_DESC = "extra_photo_desc";
        private final String SLASH = "/";
        private final String CONTENT_TYPE_HEADER = "Content-Type";
        private final String CONTENT_TYPE_HEADER_VALUE = "application/x-www-form-urlencoded";
        private final String CONTENT_TYPE_JSON_HEADER_VALUE = "application/json";
        private final String AUTHORIZATION_HEADER = "Authorization";

	
    public String savePhoto(String projectId, String resultSheetPhoto, 
            String electionPhoto1, String electionPhoto2, String photoDescription) throws IOException {
        Map<String, String> map = new LinkedHashMap<String, String>();
        map.put(ID, encode(projectId));
        map.put(RESULT_SHEET_PHOTO, encode(resultSheetPhoto));
        map.put(ELECTION_PHOTO_1, encode(electionPhoto1));
        map.put(ELECTION_PHOTO_2, encode(electionPhoto2));
        map.put(EXTRA_PHOTO_DESC, encode(photoDescription));

        String url = PHOTO_RESOURCE_ENDPOINT;

        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();
        con.setRequestMethod("PUT");
        //con.setRequestProperty("User-Agent", USER_AGENT);
        con.setRequestProperty(CONTENT_TYPE_HEADER, CONTENT_TYPE_HEADER_VALUE);        
        con.setDoOutput(true);
        
        OutputStream wr = con.getOutputStream();
        String data = getData(map);
        System.out.println("DATA String: " + data);
        wr.write(data.getBytes("UTF-8"));
        
        int responseCode = con.getResponseCode();
        System.out.println("Sending 'PUT' request to URL : " + url);
        System.out.println("Response Code : " + responseCode);

        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        return response.toString();
    }   
     
    private static String getData(Map<String, String> map) {
        StringBuilder sb = new StringBuilder();
        for (Map.Entry<String, String> entry : map.entrySet()) {
            sb.append(entry.getKey());
            sb.append('=');
            sb.append(entry.getValue());
            sb.append('&');
        }
        if (sb.length() > 0) {
            sb.setLength(sb.length() - 1);  
        }
        
        return sb.toString(); 
    }
            
    private static String encode(String value)
            throws UnsupportedEncodingException {
        if (value == null) {         
            return "";
        }
        return URLEncoder.encode(value.trim(), StandardCharsets.UTF_8.name());
    }    

}