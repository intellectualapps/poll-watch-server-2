/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pw.app.manager;

import com.pw.app.pojo.AppBoolean;
import com.pw.app.util.exception.GeneralAppException;

/**
 *
 * @author buls
 */
public interface PartyScoreManagerLocal {
    
    AppBoolean savePartyScores(String projectId, String partyScores, String rawToken)
            throws GeneralAppException;
    
}