/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pw.app.manager;

import com.pw.app.pojo.AppBoolean;
import com.pw.app.util.exception.GeneralAppException;

/**
 *
 * @author buls
 */
public interface ProjectManagerLocal {
    
    AppBoolean saveProject(String projectId, String agentId, String puId, String electionId, 
            String latitude, String longitude, String altitude, String accuracy, 
            String ec8aUrl, String registeredVoters, String accreditedVoters, 
            String validVotes, String rejectedVotes, String deviceId, String simSerial, 
            String metaInstanceId, String nullificationReason, String rawToken) throws GeneralAppException ;
   
}