/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pw.app.manager;

import com.pw.app.data.manager.ExceptionThrowerManagerLocal;
import com.pw.app.model.DeviceToken;
import com.pw.app.model.User;
import com.pw.app.pojo.AppBoolean;
import com.pw.app.pojo.AppUser;
import com.pw.app.pojo.UserPayload;
import com.pw.app.util.CodeGenerator;
import com.pw.app.util.JWT;
import com.pw.app.util.JikaDecode;
import com.pw.app.util.MD5;
import com.pw.app.util.SocialPlatformType;
import com.pw.app.util.Verifier;
import java.util.Calendar;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import com.pw.app.util.exception.GeneralAppException;
import io.jsonwebtoken.Claims;
import java.util.ArrayList;
import java.util.logging.Logger;
import com.pw.app.data.manager.UserDataManagerLocal;
import com.pw.app.pojo.AppDataAccess;
import com.pw.app.pojo.AppParty;
import com.pw.app.pojo.UserDataAccessPayload;

/**
 *
 * @author Lateefah
 */
@Stateless
public class UserManager implements UserManagerLocal {

    @EJB
    private UserDataManagerLocal userDataManager;
    
    @EJB
    private DataAccessManagerLocal dataAccessManager;
    
    @EJB
    private ExceptionThrowerManagerLocal exceptionManager;

    @EJB
    private DeviceTokenManagerLocal deviceTokenManager;
    
    @EJB
    private PartyManagerLocal partyManager;
    
    @EJB
    private CodeGenerator codeGenerator;
    
    @EJB
    private Verifier verifier;
    
    private final String USER_LINK = "/user";
    private final String AUTHENTICATE_USER_LINK = "/user/authenticate";
    private final String VERIFY_USERNAME_LINK = "/user/verify";
    private final String CREATE_USERNAME_LINK = "/user/username";
    private final String LINK_SOCIAL_ACCOUNT = "/user/link-social";
    private final String UNLINK_SOCIAL_ACCOUNT = "/user/unlink-social";    
    private final String NOTIFY_OFFLINE_USER_LINK = "/message/notify";
    private final String DEVICE_TOKEN_LINK = "/user/device-token";
    private final String RESET_USER_PASSWORD_LINK = "/user/password";
    private final String PASSWORD_RESET_TOKEN_LINK = "/password-token";
    private final long TOKEN_LIFETIME= 31556952000l;
    private final long PUBLISH_TIMEOUT = 1000;
    
    private Logger logger = Logger.getLogger(UserManager.class.getName());

          
    @Override
    public AppUser register(String username, String email, String password) throws GeneralAppException {

        verifier.setResourceUrl(USER_LINK)
                .verifyParams(username, email, password);        
        
        if (!verifier.isValidEmailAddress(email)) {
            exceptionManager.throwInvalidEmailFormatException(USER_LINK);
        }                
        
        if (userDataManager.get(username) != null) {
            exceptionManager.throwUserAlreadyExistException(USER_LINK);
        }                

        User user = new User();

        user.setUsername(username);        
        user.setCreationDate(Calendar.getInstance().getTime());
        user.setPassword(MD5.hash(password));
      
     
        return getAppUserWithToken(getAppUser(userDataManager.create(user)));
    }
    
    @Override
    public AppUser updateUserDetails(String username, String email, String firstName, 
            String lastName, String phoneNumber, String photoUrl, String location, 
            String facebookEmail, String linkedinEmail, String shortBio, String longBio, String facebookProfile,
            String linkedinProfile, String receiveEmails, String receivePushNotifications, String rawToken) throws GeneralAppException {
        
        verifier.setResourceUrl(USER_LINK)
                .verifyParams(rawToken);
        
        Claims claims = verifier.setResourceUrl(USER_LINK)
                .verifyJwt(rawToken);
        
        if (username == null) {
            username = claims.getSubject();
        }
        
        logger.info("username from update profile: " + username);
        
        User user = userDataManager.get(username);
        
        
        if (user == null) {
            exceptionManager.throwUserDoesNotExistException(USER_LINK);
        }
        
        
        
    
        User updatedUser = userDataManager.update(user);
        
        
        AppUser appUser = getAppUser(updatedUser);
        
        return appUser;
    }
    
    @Override
    public AppBoolean deleteUser(String username) throws GeneralAppException {

        verifier.setResourceUrl(USER_LINK)
                .verifyParams(username);

        User user = userDataManager.get(username);
        if (user == null) {
            exceptionManager.throwUserDoesNotExistException(USER_LINK);
        }
        
        //catch database errors here
        userDataManager.delete(user);
        
        return getAppBoolean(true);
    }
    
    @Override
    public AppUser getUserDetails (String username, String rawToken) throws GeneralAppException {
        
        verifier.setResourceUrl(USER_LINK)
                .verifyParams(username, rawToken);
        
        verifier.setResourceUrl(USER_LINK)
                .verifyJwt(rawToken);
        
        AppUser appUser = null;
        
        try {
           appUser = getAppUser(userDataManager.get(username));
        } catch (Exception e) {
            exceptionManager.throwUserDoesNotExistException(USER_LINK);
        }
        
        return appUser;
    }
    
    @Override
    public AppUser getUserDetailsWithDeviceToken (String username, String rawToken) throws GeneralAppException {
        
        verifier.setResourceUrl(USER_LINK)
                .verifyParams(username, rawToken);
        
        verifier.setResourceUrl(USER_LINK)
                .verifyJwt(rawToken);
        
        AppUser appUser = null;
        
        try {
           appUser = getAppUser(userDataManager.get(username));
           appUser = getAppUserWithDeviceToken(appUser);
        } catch (Exception e) {
            exceptionManager.throwUserDoesNotExistException(USER_LINK);
        }
        
        return appUser;
    }
    
    @Override
    public AppUser getUserDetails (String rawToken) throws GeneralAppException {
        
        verifier.setResourceUrl(USER_LINK)
                .verifyParams(rawToken);               
        
         Claims claims = verifier.setResourceUrl(USER_LINK)
                .verifyJwt(rawToken);
        
        String username =  claims.getSubject();
        
        if (username == null) {
            exceptionManager.throwInvalidTokenException(USER_LINK);
        }
        logger.info("username in token: " + username);
        
        AppUser appUser = null;
        
        try {
           appUser = getAppUser(userDataManager.get(username));
        } catch (Exception e) {
            exceptionManager.throwUserDoesNotExistException(USER_LINK);
        }
        
        return appUser;
    }
     
    @Override
    public UserDataAccessPayload authenticate (String id, String password, String type) throws GeneralAppException {
        
        AppUser appUser = null;
        if (type.equals(SocialPlatformType.EMAIL.getDescription())) {
            verifier.setResourceUrl(AUTHENTICATE_USER_LINK)
                    .verifyParams(id, password, type);
        } else {
            verifier.setResourceUrl(AUTHENTICATE_USER_LINK)
                    .verifyParams(id, type);
        }
                
        UserDataAccessPayload userDataAccessPayload = new UserDataAccessPayload();
        if (type.equals(SocialPlatformType.USERNAME.getDescription())) {
                
            User user = userDataManager.getByUsername(id).isEmpty() ? null : userDataManager.getByUsername(id).get(0);
            if (user == null) {
                exceptionManager.throwUserDoesNotExistException(AUTHENTICATE_USER_LINK);
            } else {
                if (user.getPassword() == null || !(user.getPassword().equals(password))) {
                    exceptionManager.throwInvalidLoginCredentialsException(AUTHENTICATE_USER_LINK);
                }
                
                appUser = getAppUserWithToken(getAppUser(user));
                List<AppDataAccess> userElectionEvents = dataAccessManager.getDataAccess(user.getUsername());  
                List<AppParty> parties = partyManager.getAppParties();
                userDataAccessPayload.setUser(appUser);
                userDataAccessPayload.setElectionEvents(userElectionEvents);
                userDataAccessPayload.setParties(parties);
            }        
        } else {
            User user = userDataManager.get(id);
            
            if (user == null) {
                exceptionManager.throwUserDoesNotExistException(AUTHENTICATE_USER_LINK);
            }
            
            if (user.getPassword() == null || user.getPassword().isEmpty()) {
                exceptionManager.throwNoEmailLoginAllowedException(AUTHENTICATE_USER_LINK);
            }
            
            if (!(user != null && user.getPassword().equals(MD5.hash(password)))) {
                exceptionManager.throwInvalidLoginCredentialsException(AUTHENTICATE_USER_LINK);
            }
            
            appUser = getAppUserWithToken(getAppUser(user));
            List<AppDataAccess> userElectionEvents = dataAccessManager.getDataAccess(user.getUsername());
            List<AppParty> parties = partyManager.getAppParties();
            userDataAccessPayload.setUser(appUser);
            userDataAccessPayload.setElectionEvents(userElectionEvents);
            userDataAccessPayload.setParties(parties);
        }                     
        
        return userDataAccessPayload;
    }
    
    @Override
    public AppBoolean verifyUsername(String username) throws GeneralAppException{
        verifier.setResourceUrl(VERIFY_USERNAME_LINK)
                .verifyParams(username);
        
        User user = userDataManager.get(username);
        if (user == null ) {
            return getAppBoolean(true);
        }
        return getAppBoolean(false);
    }
    
    @Override
    public AppUser createUsername(String username, String email, String tempKey) throws GeneralAppException{        
                
        verifier.setResourceUrl(CREATE_USERNAME_LINK)
                .verifyParams(username, email, tempKey);
                
        if (!verifier.isValidEmailAddress(email)) {
            exceptionManager.throwInvalidEmailFormatException(CREATE_USERNAME_LINK);
        }                
        
        if (userDataManager.get(username) != null) {
            exceptionManager.throwUserAlreadyExistException(CREATE_USERNAME_LINK);
        }
        
        User existingUser = userDataManager.get(tempKey); 
        
        if (existingUser == null) {
            exceptionManager.throwUserDoesNotExistException(CREATE_USERNAME_LINK);
        }
        
        User user = new User();
        
        user.setCreationDate(existingUser.getCreationDate());
        user.setUsername(username);
        
        userDataManager.delete(existingUser);
        
        AppUser appUser = getAppUser(userDataManager.create(user));
        return getAppUserWithToken(appUser);

    }
    
    @Override
    public AppUser linkSocialAcount(String username, String platform, String email, String rawToken) throws GeneralAppException {
        
        User user = null;
        
        verifier.setResourceUrl(LINK_SOCIAL_ACCOUNT)
                .verifyParams(username, platform, email, rawToken);
        
        verifier.setResourceUrl(LINK_SOCIAL_ACCOUNT)
                .verifyJwt(rawToken);
        
        user = userDataManager.get(username);
                
        if ( user != null ) {
            if (platform.equals(SocialPlatformType.FACEBOOK.getDescription())) {
                
            } else if (platform.equals(SocialPlatformType.LINKEDIN.getDescription())) {
                
            } else {
                exceptionManager.throwInvalidSocialPlatform(LINK_SOCIAL_ACCOUNT);
            }
        
        } else {
            exceptionManager.throwUserDoesNotExistException(LINK_SOCIAL_ACCOUNT);
        }        
        return getAppUser(userDataManager.update(user));

    }
    
    @Override
    public AppUser unlinkSocialAcount(String username, String platform, String rawToken) throws GeneralAppException {
        
        User user = null;
        
        verifier.setResourceUrl(UNLINK_SOCIAL_ACCOUNT)
                .verifyParams(username, platform, rawToken);
        
        verifier.setResourceUrl(UNLINK_SOCIAL_ACCOUNT)
                .verifyJwt(rawToken);
        
        user = userDataManager.get(username);
                
        if ( user != null ) {
            if (platform.equals(SocialPlatformType.FACEBOOK.getDescription())) {
                
            } else if (platform.equals(SocialPlatformType.LINKEDIN.getDescription())) {
                
            } else {
                exceptionManager.throwInvalidSocialPlatform(UNLINK_SOCIAL_ACCOUNT);
            }
        
        } else {
            exceptionManager.throwUserDoesNotExistException(UNLINK_SOCIAL_ACCOUNT);
        }        
        return getAppUser(userDataManager.update(user));

    }
    
    private AppUser getAppUser(User user) {
        AppUser appUser = new AppUser();
        
        //set a temp key if username auto generated.        
        appUser.setTempKey(user.getUsername().startsWith("autogen_") ? user.getUsername() : null);
        
        //if username autogenerated do not set as username
        String username = user.getUsername().startsWith("autogen_") ? null : user.getUsername();
        appUser.setId(user.getId());
        appUser.setUsername(username);
        appUser.setCreationDate(user.getCreationDate());        
        
        return appUser;
    }
        
    private AppUser getAppUserWithToken(AppUser appUser) {
        JWT tokenGenerator = new JWT();
        String authToken = tokenGenerator.createJWT(appUser, TOKEN_LIFETIME);
        appUser.setAuthToken(authToken);
        return appUser;
    }
    
    private AppUser getAppUserWithDeviceToken(AppUser appUser) {
        DeviceToken deviceToken = deviceTokenManager.getDeviceToken(appUser.getUsername());
        if (deviceToken != null) {
            appUser.setDeviceToken(deviceToken.getToken());
            appUser.setDevicePlatform(deviceToken.getPlatform());
            appUser.setUuid(deviceToken.getUuid());
        }
        
        return appUser;
    }
    
    private AppBoolean getAppBoolean(Boolean status) {
        AppBoolean appBoolean = new AppBoolean();
        
        appBoolean.setStatus(status);
        
        return appBoolean;
    }                

    @Override
    public UserPayload getListOfUsers(String usernames, String rawToken) throws GeneralAppException {
        verifier.setResourceUrl(USER_LINK).verifyParams(usernames, rawToken);
        verifier.setResourceUrl(USER_LINK).verifyJwt(rawToken);
        
        List<AppUser> usersDetails = new ArrayList<AppUser>();
        String[] listOfUsernames = usernames.split("\\s*,\\s*");
        for (int i = 0; i < listOfUsernames.length; i++) {
            usersDetails.add(getUserDetails(listOfUsernames[i], rawToken));
        }
        
        UserPayload users = new UserPayload();
        users.setUsers(usersDetails);
        
        return users;
    }
    
    @Override
    public UserPayload getListOfUsersWithDeviceToken(String usernames, String rawToken) throws GeneralAppException {
        verifier.setResourceUrl(USER_LINK).verifyParams(usernames, rawToken);
        verifier.setResourceUrl(USER_LINK).verifyJwt(rawToken);
        
        List<AppUser> usersDetails = new ArrayList<AppUser>();
        String[] listOfUsernames = usernames.split("\\s*,\\s*");
        for (int i = 0; i < listOfUsernames.length; i++) {
            usersDetails.add(getUserDetailsWithDeviceToken(listOfUsernames[i], rawToken));
        }
        
        UserPayload users = new UserPayload();
        users.setUsers(usersDetails);
        
        return users;
    }
    
    @Override
    public void notifyOfflineUser(String offlineUsername, String onlineUsername, 
            String connectionId, String rawToken, String notificationPlatform) throws GeneralAppException {
        verifier.setResourceUrl(NOTIFY_OFFLINE_USER_LINK)
                .verifyParams(offlineUsername, onlineUsername, connectionId, rawToken);
        verifier.verifyJwt(rawToken);
        
        User onlineUser = userDataManager.get(onlineUsername);        
        if (onlineUser == null) {
            exceptionManager.throwUserDoesNotExistException(DEVICE_TOKEN_LINK);
        }
                
        DeviceToken offlineUserDeviceToken = deviceTokenManager.getDeviceToken(offlineUsername);
        String offlineUserNotificationPlatform = "";
        if (offlineUserDeviceToken != null) {
            offlineUserNotificationPlatform = offlineUserDeviceToken.getPlatform();            
        } else {
            return;
        }
        
        if (offlineUserNotificationPlatform != null) {
            if (offlineUserNotificationPlatform.equals("1")) { 
                notificationPlatform = "android"; 
            } else if (offlineUserNotificationPlatform.equals("2")) { 
                notificationPlatform = "ios";
            }
           
        }
        
    }
    
    
    @Override
    public DeviceToken saveDeviceToken(String username, String token, String uuid, 
            String platform, String rawToken) 
            throws GeneralAppException {
                
        verifier.setResourceUrl(DEVICE_TOKEN_LINK)
                .verifyParams(username, token, uuid, rawToken);
        verifier.verifyJwt(rawToken);
        
        
        User user = userDataManager.get(username);
        if (user == null) {
            exceptionManager.throwUserDoesNotExistException(DEVICE_TOKEN_LINK);
        }
        
        DeviceToken existingToken = deviceTokenManager.getDeviceToken(username);
        if (existingToken != null) {
            existingToken.setToken(token);
            existingToken.setUuid(uuid);
            existingToken.setPlatform(platform);
            return deviceTokenManager.updateDeviceToken(existingToken);
        } else {
            DeviceToken deviceToken = new DeviceToken();
            deviceToken.setUsername(username);
            deviceToken.setToken(token);
            deviceToken.setUuid(uuid);
            deviceToken.setPlatform(platform);
            return deviceTokenManager.createDeviceToken(deviceToken);
        }
    }
     
    @Override
    public AppUser resetPassword (String email) 
            throws GeneralAppException {
        return null;
    }
    
    @Override
    public AppUser setNewPassword (String encodedEmail, String password)
            throws GeneralAppException {        
        
        return null;
    }
    
    @Override
    public String authenticatePasswordResetToken(String encodedEmail, String token) 
            throws GeneralAppException {     
        return Boolean.TRUE.toString();

    }
    
}
