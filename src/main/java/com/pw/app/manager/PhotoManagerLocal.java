/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pw.app.manager;

import com.pw.app.pojo.AppBoolean;
import com.pw.app.util.exception.GeneralAppException;


public interface PhotoManagerLocal {     
    AppBoolean savePhotoData (String projectId, String resultSheetPhoto, 
            String electionPhoto1, String electionPhoto2, String photoDescription) throws GeneralAppException;    
}