/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pw.app.data.manager;

import com.pw.app.model.DeviceToken;
import javax.ejb.EJBTransactionRolledbackException;
import javax.ejb.Local;

/**
 *
 * @author buls
 */
@Local
public interface DeviceTokenDataManagerLocal {
    
    DeviceToken create(DeviceToken deviceToken) throws EJBTransactionRolledbackException;

    DeviceToken update(DeviceToken deviceToken);

    DeviceToken get(String userId);        

    void delete(DeviceToken deviceToken);    

}
