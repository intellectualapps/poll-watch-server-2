/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pw.app.data.manager;

import com.pw.app.model.User;
import java.util.List;
import javax.ejb.EJBTransactionRolledbackException;
import javax.ejb.Local;

/**
 *
 * @author buls
 */
@Local
public interface UserDataManagerLocal {
    
    User create(User user) throws EJBTransactionRolledbackException;

    User update(User user);

    User get(String userId);

    void delete(User user);
    
    List<User> getByUsername(String username);
}
