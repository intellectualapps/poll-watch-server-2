/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pw.app.data.manager;

import com.pw.app.model.DataAccess;
import java.util.List;
import javax.ejb.EJBTransactionRolledbackException;
import javax.ejb.Local;

/**
 *
 * @author buls
 */
@Local
public interface DataAccessDataManagerLocal {
    
    DataAccess create(DataAccess dataAccess) throws EJBTransactionRolledbackException;

    DataAccess update(DataAccess dataAccess);

    DataAccess get(String dataAccessId);

    void delete(DataAccess dataAccess);
    
    List<DataAccess> getByUsername(String username);
}
