/*
 * To change this license header, choose License Headers in PartyScore Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pw.app.data.manager;

import com.pw.app.data.provider.DataProviderLocal;
import com.pw.app.model.Party;
import com.pw.app.model.PartyScore;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.EJBTransactionRolledbackException;
import javax.ejb.Stateless;

/**
 *
 * @author buls
 */
@Stateless
public class PartyDataManager implements PartyDataManagerLocal {

    @EJB
    private DataProviderLocal crud;    
    
    @Override
    public List<Party> get() {
        return crud.findAll(Party.class);
    }
  
}

