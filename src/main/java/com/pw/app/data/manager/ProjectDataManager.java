/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pw.app.data.manager;

import com.pw.app.data.provider.DataProviderLocal;
import com.pw.app.model.Project;
import java.util.HashMap;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.EJBTransactionRolledbackException;
import javax.ejb.Stateless;

/**
 *
 * @author buls
 */
@Stateless
public class ProjectDataManager implements ProjectDataManagerLocal {

    @EJB
    private DataProviderLocal crud;    

    @Override
    public Project create(Project project) throws EJBTransactionRolledbackException {
        return crud.create(project);
    }

    @Override
    public Project update(Project project) {
        return crud.update(project);
    }

    @Override
    public Project get(String projectId) {
        return crud.find(projectId, Project.class);
    }

    @Override
    public void delete(Project project) {
        crud.delete(project);
    }
  
    @Override
    public List<Project> getByUsername(String username) {        
        HashMap<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("username", username);
        return crud.findByNamedQuery("Project.findByUsername", parameters, Project.class);
    }    
}

