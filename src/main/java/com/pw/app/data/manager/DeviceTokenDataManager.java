/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pw.app.data.manager;

import com.pw.app.data.provider.DataProviderLocal;
import com.pw.app.model.DeviceToken;
import javax.ejb.EJB;
import javax.ejb.EJBTransactionRolledbackException;
import javax.ejb.Stateless;

/**
 *
 * @author buls
 */
@Stateless
public class DeviceTokenDataManager implements DeviceTokenDataManagerLocal {

    @EJB
    private DataProviderLocal crud;    

    @Override
    public DeviceToken create(DeviceToken deviceToken) throws EJBTransactionRolledbackException {
        return crud.create(deviceToken);
    }

    @Override
    public DeviceToken update(DeviceToken deviceToken) {
        return crud.update(deviceToken);
    }

    @Override
    public DeviceToken get(String userId) {
        return crud.find(userId, DeviceToken.class);
    }

    @Override
    public void delete(DeviceToken deviceToken) {
        crud.delete(deviceToken);
    }

}

