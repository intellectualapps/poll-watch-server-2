/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pw.app.service;


import com.pw.app.manager.UserManagerLocal;
import com.pw.app.model.DeviceToken;
import com.pw.app.pojo.AppUser;
import com.pw.app.pojo.UserDataAccessPayload;
import com.pw.app.pojo.UserPayload;
import com.pw.app.util.exception.GeneralAppException;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.DELETE;
import javax.ws.rs.POST;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author Lateefah
 */
@Stateless
@Path("/v1/user")
public class UserService {
    
    @Context
    HttpServletRequest request;   
    
    @EJB    
    UserManagerLocal userManager;
       
    //http://localhost:8080/zus/api/v1/user/?username=buls&email=lateefah.abdulkareem@intellectualapps.com&password=password
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response addUser(@QueryParam("username") String username,
                            @QueryParam("email") String email,
                            @QueryParam("password") String password) throws GeneralAppException {  
        
        AppUser appUser = userManager.register(username, email, password);
        return Response.ok(appUser).build();
           
    }
        
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getUser(@HeaderParam("Authorization") String rawToken) throws GeneralAppException {  
        
        AppUser appUser = userManager.getUserDetails(rawToken);
        return Response.ok(appUser).build();
           
    }
    
    @Path("/{username}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getUser(@HeaderParam("Authorization") String rawToken,
                            @PathParam("username") String username) throws GeneralAppException {  
        
        AppUser appUser = userManager.getUserDetails(username, rawToken);
        return Response.ok(appUser).build();
           
    }
    
    @Path("/{username}")
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateUser(@PathParam("username") String username,
                                @QueryParam("email") String email,
                                @QueryParam("location") String location,
                                @QueryParam("first-name") String firstName,
                                @QueryParam("last-name") String lastName,
                                @QueryParam("photo-url") String photoUrl,
                                @QueryParam("phone-number") String phoneNumber,
                                @QueryParam("facebook-email") String facebookEmail,
                                @QueryParam("linkedin-email") String linkedinEmail,
                                @QueryParam("short-bio") String shortBio,
                                @QueryParam("long-bio") String longBio,
                                @QueryParam("facebook-profile") String facebookProfile,
                                @QueryParam("linkedin-profile") String linkedinProfile,
                                @QueryParam("receive-emails") String receiveEmails, 
                                @QueryParam("receive-push") String receivePushNotifications, 
                                @HeaderParam("Authorization") String rawToken) throws GeneralAppException {  
        
        AppUser appUser = userManager.updateUserDetails(username, email, firstName, lastName, phoneNumber, photoUrl, 
                                                        location, facebookEmail, linkedinEmail, shortBio, longBio, 
                                                        facebookProfile, linkedinProfile, receiveEmails, receivePushNotifications, rawToken);
        return Response.ok(appUser).build();
           
    }
        
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateUserBasedOnAuthToken(@QueryParam("email") String email,
                                @QueryParam("location") String location,
                                @QueryParam("first-name") String firstName,
                                @QueryParam("last-name") String lastName,
                                @QueryParam("photo-url") String photoUrl,
                                @QueryParam("phone-number") String phoneNumber,
                                @QueryParam("facebook-email") String facebookEmail,
                                @QueryParam("linkedin-email") String linkedinEmail,
                                @QueryParam("short-bio") String shortBio,
                                @QueryParam("long-bio") String longBio,
                                @QueryParam("facebook-profile") String facebookProfile,
                                @QueryParam("linkedin-profile") String linkedinProfile,
                                @QueryParam("receive-emails") String receiveEmails, 
                                @QueryParam("receive-push") String receivePushNotifications, 
                                @HeaderParam("Authorization") String rawToken) throws GeneralAppException {  
        
        AppUser appUser = userManager.updateUserDetails(null, email, firstName, lastName, phoneNumber, photoUrl, 
                                                        location, facebookEmail, linkedinEmail, shortBio, longBio, 
                                                        facebookProfile, linkedinProfile, receiveEmails, receivePushNotifications,rawToken);
        return Response.ok(appUser).build();
           
    }
    
    @Path("/{username}")
    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteUser(@PathParam("username") String username) throws GeneralAppException {  
        
        return Response.ok(userManager.deleteUser(username)).build();
           
    }
    
    @Path("/authenticate")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response authenticateUser(@QueryParam("id") String id,
                            @QueryParam("password") String password,
                            @QueryParam("auth-type") String type) throws GeneralAppException {  
        
        UserDataAccessPayload userDataAccessPayload = userManager.authenticate(id, password, type);
        return Response.ok(userDataAccessPayload).build();
           
    }
    
    @Path("/username")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response createUsername(@QueryParam("username") String username,
                            @QueryParam("email") String email, 
                            @QueryParam("temp-key") String tempKey) throws GeneralAppException {  
        
        AppUser appUser = userManager.createUsername(username, email, tempKey);
        return Response.ok(appUser).build();
           
    }
    
    @Path("/verify/{username}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response verifyUsername(@PathParam("username") String username) throws GeneralAppException {   
        
        return Response.ok(userManager.verifyUsername(username)).build();            
    
    }
    
    @Path("/link-social/{username}")
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public Response linkSocialAccount(@PathParam("username") String username,
                            @QueryParam("platform") String platform,
                            @QueryParam("email") String email,
                            @HeaderParam("Authorization") String rawToken) throws GeneralAppException {  
        
        AppUser appUser = userManager.linkSocialAcount(username, platform, email, rawToken);
        return Response.ok(appUser).build();
           
    }
    
    @Path("/unlink-social/{username}")
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public Response unlinkSocialAccount(@PathParam("username") String username,
                            @QueryParam("platform") String platform,
                            @HeaderParam("Authorization") String rawToken) throws GeneralAppException {  
        
        AppUser appUser = userManager.unlinkSocialAcount(username, platform, rawToken);
        return Response.ok(appUser).build();
           
    }

    @Path("/get-list")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getListOfUsers(@HeaderParam("Authorization") String rawToken,
                            @QueryParam("usernames") String usernames) throws GeneralAppException {  
        
        UserPayload appUser = userManager.getListOfUsers(usernames, rawToken);
        return Response.ok(appUser).build();
           
    }
    
    @Path("/list-with-token")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getListOfUsersWithDeviceToken(@HeaderParam("Authorization") String rawToken,
                            @QueryParam("usernames") String usernames) throws GeneralAppException {  
        
        UserPayload appUser = userManager.getListOfUsersWithDeviceToken(usernames, rawToken);
        return Response.ok(appUser).build();
           
    }
    
    @Path("/notify")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response notifyUser(@QueryParam("offline-username") String offlineUsername,
            @QueryParam("online-username") String onlineUsername,
            @QueryParam("message-id") String messageId,
            @QueryParam("platform") String notificationPlatform,
            @HeaderParam("Authorization") String rawToken)
    throws GeneralAppException {  
        userManager.notifyOfflineUser(offlineUsername, onlineUsername, messageId, 
                rawToken, notificationPlatform);        
        return Response.ok().build();
           
    }
    
    @Path("/device-token/{username}")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response saveDeviceToken(@PathParam("username") String username,
            @QueryParam("device-token") String deviceToken, @QueryParam("uuid") String uuid,
            @QueryParam("platform") String platform, 
            @HeaderParam("Authorization") String rawToken)
    throws GeneralAppException {  
        DeviceToken userDeviceToken = userManager.saveDeviceToken(
                username, deviceToken, uuid, platform, rawToken);        
        return Response.ok(userDeviceToken).build();
           
    }
    
    @Path("/password")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response resetUserPassword(@QueryParam("email") String email) 
            throws GeneralAppException {  
        
        AppUser appUser = userManager.resetPassword(email) ;
        return Response.ok(appUser).build();
           
    }
    
    @Path("/password")
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public Response setNewPassword(@QueryParam("email") String email, 
            @QueryParam("password") String password) 
            throws GeneralAppException {  
        
        AppUser appUser = userManager.setNewPassword(email, password);
        return Response.ok(appUser).build();
           
    }
    
    @Path("/password-token")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response authenticatePasswordResetToken(@QueryParam("email") String email,
            @QueryParam("password-reset-token") String token) 
            throws GeneralAppException {  
        
        String authentic = userManager.authenticatePasswordResetToken(email, token) ;
        return Response.ok(authentic).build();
           
    }
    
}
